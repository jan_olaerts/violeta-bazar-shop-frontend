export enum Payment {

  PAYMENT_ERROR_QUERY_STRING = '?payment=fail',
  PAYMENT_PENDING_QUERY_STRING = '?payment=pending',
  PAYMENT_SUCCESS_QUERY_STRING = '?payment=success'
}
