export enum ProfileConstant {

  MY_ORDERS = 'Mis Compras',
  ALL_ORDERS = 'Ordenes',
  USERS = 'Usuarios',
  UPDATE_ACCOUNT = 'Actualizar Cuenta',
  LOGOUT = 'Salir',
  MY_ORDERS_URL = '/profile/my-orders',
  ALL_ORDERS_URL = '/profile/all-orders',
  USERS_URL = '/profile/users',
  UPDATE_ACCOUNT_URL = '/profile/update-account',
  LOGOUT_URL = '/profile/logout'
}
