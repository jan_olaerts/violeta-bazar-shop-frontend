export enum Authority {

  USER_READ = 'user:read',
  USER_SEARCH = 'user:search',
  USER_DELETE = 'user:delete'
}
