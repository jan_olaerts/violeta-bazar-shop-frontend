import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEditProductComponent } from './components/products/add-edit-product/add-edit-product.component';
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { ShowProductsComponent } from './components/products/show-products/show-products.component';
import { LoginComponent } from './components/users/login/login.component';
import { RegisterComponent } from './components/users/register/register.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { NotFoundComponent } from './components/navigation/notfound/notfound.component';

const routes: Routes = [
  { path: '', component: ShowProductsComponent, pathMatch: 'full' },
  { path: 'category/:id', component: ShowProductsComponent },
  { path: 'search/:keyword', component: ShowProductsComponent },
  { path: 'addproduct', component: AddEditProductComponent },
  { path: 'editproduct/:id', component: AddEditProductComponent },
  { path: 'productdetails/:id', component: ProductDetailsComponent },
  { path: 'shoppingcart', component: ShoppingCartComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile/:action', component: ProfileComponent, canActivate: [AuthenticationGuard] },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
