import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private productBaseUrl = 'http://localhost:8080/api/products';
  private productCategoryBaseUrl = "http://localhost:8080/api/productcategories";
  private userBaseUrl = 'http://localhost:8080/api/users';

  constructor(private authenticationService: AuthenticationService) { }

  intercept(httpRequest: HttpRequest<unknown>, httpHandler: HttpHandler): Observable<HttpEvent<unknown>> {

    // eliminating requests that we do not want to add a token to
    if (httpRequest.url.includes(this.productBaseUrl + '/getById/')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.productBaseUrl + '/getAll')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.productBaseUrl + '/byCategoryId/')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.productBaseUrl + '/lookup')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.productBaseUrl + '/byCartItems')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.productCategoryBaseUrl)) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.userBaseUrl + '/register')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.userBaseUrl + '/login')) {
      return httpHandler.handle(httpRequest);
    }

    if (httpRequest.url.includes(this.userBaseUrl + '/getPerson')) {
      return httpHandler.handle(httpRequest);
    }

    // adding token to the headers
    this.authenticationService.loadToken();
    const token = this.authenticationService.getToken();
    const request: HttpRequest<any> = httpRequest.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
    return httpHandler.handle(request);
  }
}
