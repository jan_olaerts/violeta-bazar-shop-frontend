import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/navigation/header/header.component';
import { AddEditProductComponent } from './components/products/add-edit-product/add-edit-product.component';
import { SideMenuComponent } from './components/navigation/side-menu/side-menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ShowProductsComponent } from './components/products/show-products/show-products.component';
import { ProductDetailsComponent } from './components/products/product-details/product-details.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { LoginComponent } from './components/users/login/login.component';
import { RegisterComponent } from './components/users/register/register.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { NotFoundComponent } from './components/navigation/notfound/notfound.component';
import { OrdersOverviewComponent } from './components/users/profile/orders-overview/orders-overview.component'
import { LogoutComponent } from './components/users/profile/logout/logout.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AuthenticationGuard } from './guards/authentication.guard';
import { UsersOverviewComponent } from './components/users/profile/users-overview/users-overview.component';
import { UpdateAccountComponent } from './components/users/profile/update-account/update-account.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AddEditProductComponent,
    SideMenuComponent,
    ShowProductsComponent,
    ProductDetailsComponent,
    ShoppingCartComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    NotFoundComponent,
    OrdersOverviewComponent,
    LogoutComponent,
    UsersOverviewComponent,
    UpdateAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthenticationGuard, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
