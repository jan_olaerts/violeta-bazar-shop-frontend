import { Address } from "./address";
import { CartItem } from "./cart-item";
import { User } from "./user";

export class Order {

  id: number;
  placementTime: Date;
  user: User;
  send_firstNames: string;
  send_lastNames: string;
  send_address: Address;
  cartItems: CartItem[];
  totalPrice: number;

  constructor(id: number, placementTime: Date, user: User, send_firstNames: string, send_lastNames: string,
    send_address: Address, cartItems: CartItem[], totalPrice: number) {

    this.id = id;
    this.placementTime = placementTime;
    this.user = user;
    this.send_firstNames = send_firstNames;
    this.send_lastNames = send_lastNames;
    this.send_address = send_address;
    this.cartItems = cartItems;
    this.totalPrice = totalPrice;
  }
}
