export class Address {

  id: number;
  street: string;
  number: string;
  city: string;
  department: string;
  province: string;

  constructor(id: number, street: string, number: string, city: string, province: string, department: string) {
    this.id = id;
    this.street = street;
    this.number = number;
    this.city = city;
    this.province = province;
    this.department = department;
  }
}
