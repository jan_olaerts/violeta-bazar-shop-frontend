import { Address } from "src/app/classes/address";
import { Order } from "./order";


export class User {

  email: string;
  password: string;
  dni: string;
  firstNames: string;
  lastName_1: string;
  lastName_2: string;
  address: Address;
  lastLoginDate: Date;
  lastLoginDateDisplay: Date;
  joinDate: Date;
  role: string;
  authorities: string[];
  notLocked: boolean;
  isActive: boolean;
  orders: Order[];

  constructor(email: string,
    password: string,
    dni: string,
    firstNames: string,
    lastName_1: string,
    lastName_2: string,
    address: Address,
    orders: Order[]) {

    this.email = email;
    this.password = password;
    this.dni = dni;
    this.firstNames = firstNames;
    this.lastName_1 = lastName_1;
    this.lastName_2 = lastName_2;
    this.address = address;
    this.orders = orders;
  }
}
