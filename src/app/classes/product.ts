import { ProductCategory } from './product-category';

export class Product {

  id: number;
  name: string;
  brand: string;
  description: string;
  price: number;
  imageUrl: string;
  stock: number;
  category: ProductCategory;

  constructor(id: number, name: string, brand: string, description: string, price: number,
              imageUrl: string, stock: number, category: ProductCategory) {

    this.id = id;
    this.name = name;
    this.brand = brand;
    this.description = description;
    this.price = price;
    this.imageUrl = imageUrl;
    this.stock = stock;
    this.category = category;
  }
}
