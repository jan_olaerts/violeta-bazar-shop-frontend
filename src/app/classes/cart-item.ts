import { Order } from './order';
import { Product } from './product';

export class CartItem {

  id: number;
  name: string;
  imageUrl: string;
  price: number;
  stock: number;
  quantity: number;
  order: Order;

  constructor(product: Product) {
    this.id = product.id;
    this.name = product.name;
    this.imageUrl = product.imageUrl;
    this.price = product.price;
    this.stock = product.stock;
    this.quantity = 0;
    this.order = null;
  }
}
