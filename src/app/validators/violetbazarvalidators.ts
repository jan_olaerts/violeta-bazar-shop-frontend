import { FormControl, ValidationErrors } from '@angular/forms';

export class VioletBazarValidators {

  // whitespace validation
  static notOnlyWhitespace(control: FormControl): ValidationErrors {

    if (control.value != null && control.value.trim().length === 0) {

      return { 'notOnlyWhitespace': true };
    } else {

      return null;
    }
  }

  // file extension validation
  static mustBePngJpgOrJpeg(control: FormControl): ValidationErrors {

    const regexp = new RegExp(/^(png|jpg|jpeg)$/);

    if (control.value != null) {

      const nameLength: number = control.value.length;
      const beginPosition: number = control.value.includes('.jpeg') ? nameLength - 4 : nameLength - 3;
      const extension: string = control.value.substring(beginPosition);

      console.log(extension);

      return (regexp.test(extension) ? null : { 'mustBePngJpgOrJpeg': true });
    } else {
      return { 'mustBePngJpgOrJpeg': true };
    }
  }

  // value cannot be 'Category:' validation
  static valueCannotBeCategory(control: FormControl): ValidationErrors {

    if (control.value != null && control.value === 'Categoría:')
      return { 'valueCannotBeCategory': true };

    else
      return null;
  }

  // value must start with a capital letter validation
  static mustStartWithCapitalLetter(control: FormControl): ValidationErrors {

    let letters: string[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

    if (control.value != null) {
      const firstLetter: string = control.value.charAt(0);
      for (let letter of letters) {
        if (letter == firstLetter) return null;
      }
    }

    return { 'mustStartWithCapitalLetter': true };
  }

  // value must be two words exactly
  static mustBeExactlyTwoWords(control: FormControl): ValidationErrors {

    if (control.value.split(' ').length === 2 && control.value.split(' ')[1] != '') {
      return null;
    } else {
      return { 'mustBeExactlyTwoWords': true };
    }
  }
}
