import { Component, OnInit } from '@angular/core';
import { toggleSideMenu } from './components/navigation/side-menu/side-menu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'violeta-bazar-shop-frontend';
  public toggleSideMenu = toggleSideMenu;
  cookiesAccepted: boolean;

  constructor() { }

  ngOnInit() {
    this.cookiesAccepted = JSON.parse(localStorage.getItem('violet-bazar-cookies-accepted'));
  }

  acceptCookies(): void {
    this.cookiesAccepted = true;
    localStorage.setItem('violet-bazar-cookies-accepted', JSON.stringify(true));
  }
}
