import { HttpClient } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { CartItem } from "../classes/cart-item";
import { User } from "../classes/user";
import { CustomHttpResponse } from "../interfaces/custom-http-response";

@Injectable({
  providedIn: 'root'
})
export class PaymentService implements OnInit {

  private paymentBaseUrl = "http://localhost:8080/api/payment";

  constructor(private http: HttpClient) { }

  ngOnInit(): void {

  }

  getPaymentUrl(user: User, cartItems: CartItem[]): Observable<string> {
    let formData: FormData = new FormData();
    formData.append('user', JSON.stringify(user));
    formData.append('cartItems', JSON.stringify(cartItems));
    return this.http.post(this.paymentBaseUrl, formData, { responseType: 'text' });
  }

  cancelPayment(firstName: string, email: string, paymentId: string): Observable<CustomHttpResponse> {
    let formData: FormData = new FormData();
    formData.append('firstName', firstName);
    formData.append('email', email);
    formData.append('payment_id', paymentId);
    return this.http.post<CustomHttpResponse>(this.paymentBaseUrl + "/cancel", formData);
  }
}
