import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private authenticationBaseUrl: string = 'http://localhost:8080/api/users';
  private token: string;
  private loggedInEmail: string;
  private jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) { }

  register(user: User): Observable<User> {
    return this.http.post<User>(this.authenticationBaseUrl + '/register', user);
  }

  login(email: string, password: string): Observable<HttpResponse<User> | HttpErrorResponse> {
    const formData: FormData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    return this.http.post<User>(this.authenticationBaseUrl + '/login', formData, { observe: 'response' });
  }

  logout(): void {
    this.token = null;
    this.loggedInEmail = null;
    localStorage.removeItem('violet-bazar-user');
    localStorage.removeItem('violet-bazar-token');
    localStorage.removeItem('violet-bazar-users');
  }

  saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('violet-bazar-token', token);
  }

  addUserToLocalStorage(user: User): void {
    localStorage.setItem('violet-bazar-user', JSON.stringify(user));
  }

  getUserFromLocalStorage(): User {
    return JSON.parse(localStorage.getItem('violet-bazar-user'));
  }

  loadToken(): void {
    this.token = localStorage.getItem('violet-bazar-token');
  }

  getToken(): string {
    return this.token;
  }

  isUserLoggedIn(): boolean {
    this.loadToken();
    if (this.token != null && this.token !== '') {
      if (this.jwtHelper.decodeToken(this.token).sub != null || '') {
        if (!this.jwtHelper.isTokenExpired(this.token)) {
          this.loggedInEmail = this.jwtHelper.decodeToken(this.token).sub;
          return true;
        }
      }
    } else {
      this.logout();
      return false;
    }
  }
}
