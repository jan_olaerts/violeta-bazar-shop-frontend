import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../classes/product';
import { map } from 'rxjs/operators';
import { CartItem } from '../classes/cart-item';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productBaseUrl = 'http://localhost:8080/api/products';

  constructor(private http: HttpClient) { }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(this.productBaseUrl + '/getById/' + id);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productBaseUrl + '/getAll');
  }

  getProductsByCategoryId(categoryId: number): Observable<Product[]> {
    return this.http.get<Product[]>(this.productBaseUrl + `/byCategoryId/${categoryId}`);
  }

  getProductsByKeyword(keyword: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.productBaseUrl + `/lookup?keyword=${keyword}`);
  }

  getProductsByCartItems(cartItems: CartItem[]): Observable<Product[]> {
    return this.http.post<Product[]>(this.productBaseUrl + '/byCartItems', cartItems);
  }

  postProduct(formData: FormData): Observable<Product> {
    return this.http.post<Product>(this.productBaseUrl + '/post', formData);
  }

  patchProduct(formData: FormData): Observable<Product> {
    return this.http.patch<Product>(this.productBaseUrl + '/edit', formData);
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.productBaseUrl + '/delete/' + id);
  }
}
