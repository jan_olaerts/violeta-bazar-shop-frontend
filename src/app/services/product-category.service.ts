import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductCategory } from '../classes/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  private productCategoryBaseUrl = "http://localhost:8080/api/productcategories";

  constructor(private http: HttpClient) { }

  getAllProductCategories(): Observable<ProductCategory[]> {
    return this.http.get<ProductCategory[]>(this.productCategoryBaseUrl);
  }
}
