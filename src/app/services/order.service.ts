import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../classes/order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private orderBaseUrl: string = 'http://localhost:8080/api/orders';

  constructor(private http: HttpClient) { }

  getOrdersByUserEmail(email: string): Observable<Order[]> {
    return this.http.get<Order[]>(`${this.orderBaseUrl}/${email}`);
  }

  getAllOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.orderBaseUrl + '/getAll');
  }

  placeOrder(order: Order, paymentId: string): Observable<Order> {
    return this.http.post<Order>(`${this.orderBaseUrl}/place?paymentId=${paymentId}`, order);
  }

  saveOrderToLocalStorage(order: Order): void {
    localStorage.setItem('violet-bazar-order', JSON.stringify(order));
  }

  getOrderFromLocalStorage(): Order {
    return JSON.parse(localStorage.getItem('violet-bazar-order'));
  }

  deleteOrderFromLocalStorage(): void {
    localStorage.removeItem('violet-bazar-order');
  }
}
