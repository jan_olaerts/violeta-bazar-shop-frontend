import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../classes/user';
import { CustomHttpResponse } from '../interfaces/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userBaseUrl: string = 'http://localhost:8080/api/users';

  constructor(private http: HttpClient) { }

  getPerson(dni: string): Observable<Object> {
    return this.http.get<Object>(this.userBaseUrl + `/getPerson/${dni}`);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userBaseUrl + '/getAll');
  }

  updateUser(user: User): Observable<User> {
    return this.http.patch<User>(this.userBaseUrl + '/update', user);
  }

  deleteUser(email: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(this.userBaseUrl + `/${email}`);
  }

  resetPassword(email: string, password: string): Observable<CustomHttpResponse> {
    const formData: FormData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    return this.http.post<CustomHttpResponse>(this.userBaseUrl + '/resetPassword', formData);
  }

  addUsersToLocalStorage(users: User[]): void {
    localStorage.setItem('violet-bazar-users', JSON.stringify(users));
  }

  getUsersFromLocalStorage(): User[] {
    if (localStorage.getItem('violet-bazar-users'))
      return JSON.parse(localStorage.getItem('violet-bazar-users'));

    return null;
  }
}
