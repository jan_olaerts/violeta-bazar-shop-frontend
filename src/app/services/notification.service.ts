import { Injectable } from '@angular/core';
import { Notification } from '../enum/notification.enum';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }

  showNotification(color: Notification, message: string): void {
    const toast = this.createNotification(color, message);
    document.querySelector(Notification.NOTIFICATION_CONTAINER_CLASS).innerHTML = toast;
    (<any>$('.toast')).toast('show');
  }

  private createNotification(color: Notification, message: string): string {
    const toast: string = `
      <div aria-live="polite" aria-atomic="true">
        <div>
          <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000" style="background-color: ${color};">
            <div class="toast-header">
              <strong class="mx-auto">${message}</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" style="color: ${color};">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    `;

    return toast;
  }
}
