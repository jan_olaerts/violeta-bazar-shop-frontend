import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CartItem } from '../classes/cart-item';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartItems: CartItem[] = [];
  totalPrice: Subject<number> = new Subject<number>();
  totalQuantity: Subject<number> = new Subject<number>();

  constructor() {
    this.cartItems = this.getCartItemsFromLocalStorage();
    if (this.cartItems == null) this.cartItems = [];
  }

  addToCart(cartItem: CartItem): boolean {

    if (!this.checkStock(cartItem)) return false;
    if (cartItem.quantity === 0) cartItem.quantity++;

    // check if we already have the item in our cart
    let alreadyExistsInCart: boolean = false;
    let existingCartItem: CartItem = undefined;

    if (this.cartItems.length > 0) {
      // find the item in the cart based on item id
      existingCartItem = this.cartItems.find(ci => ci.id === cartItem.id);

      // check if we found it
      alreadyExistsInCart = (existingCartItem !== undefined);
    }

    alreadyExistsInCart ? existingCartItem.quantity++ : this.cartItems.push(cartItem);

    // compute cart total price and total quantity
    this.computeCartTotals();

    // update cartItems in local storage
    this.updateCartItemsInLocalStorage();

    return true;
  }

  computeCartTotals(): void {

    let totalPriceValue: number = 0;
    let totalQuantityValue: number = 0;

    for (let cartItem of this.cartItems) {
      totalPriceValue += cartItem.price * cartItem.quantity;
      totalQuantityValue += cartItem.quantity;
    }

    // publish the new values ... all subscribers will receive the new data
    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);

    // update cartItems in cookies
    this.updateCartItemsInLocalStorage();
  }

  decrementQuantity(cartItem: CartItem) {

    cartItem = this.cartItems.find(ci => ci.id === cartItem.id);

    cartItem.quantity--;

    if (cartItem.quantity === 0)
      this.remove(cartItem);

    else
      this.computeCartTotals();

    // update cartItems in cookies
    this.updateCartItemsInLocalStorage();
  }

  remove(cartItem: CartItem) {

    // get index of item in the array
    const itemIndex: number = this.cartItems.findIndex(item => item.id === cartItem.id);

    // if found, remove the item from the array at the given index
    if (itemIndex > -1) {
      this.cartItems.splice(itemIndex, 1);
      this.computeCartTotals();
    }

    // update cartItems in local storage
    this.updateCartItemsInLocalStorage();
  }

  updateCartItemsInLocalStorage(): void {
    localStorage.setItem('violet-bazar-cart-items', JSON.stringify(this.cartItems));
    this.cartItems = this.getCartItemsFromLocalStorage();
  }

  getCartItemsFromLocalStorage(): CartItem[] {
    return JSON.parse(localStorage.getItem('violet-bazar-cart-items'));
  }

  logCartData(totalPriceValue: number, totalQuantityValue: number) {

    console.log('Contents of the cart');
    for (let cartItem of this.cartItems) {
      const subTotalPrice = cartItem.price * cartItem.quantity;
      console.log(`name: ${cartItem.name}, quantity=${cartItem.quantity}, unitPrice=${cartItem.price}, subTotalPrice=${subTotalPrice}`);
      console.log(`totalPrice: ${totalPriceValue.toFixed(2)}, totalQuantity: ${totalQuantityValue}`);
      console.log("----");
    }
  }

  deleteCartItemsFromLocalStorage(): void {
    this.cartItems = [];
    localStorage.setItem('violet-bazar-cart-items', JSON.stringify(this.cartItems));
  }

  private checkStock(cartItem: CartItem): boolean {
    return cartItem.quantity < cartItem.stock;
  }
}
