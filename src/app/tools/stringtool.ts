import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StringTool {

  static capitalizeFirstLetters(term: string): string {
    term = term.toLowerCase();
    let words: string[] = term.split(/[ -_]/);
    let capitalizedWords: string[] = new Array();
    let capitalizedTerm: string;

    for (let word of words) {
      word = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
      capitalizedWords.push(word);
    }

    capitalizedTerm = capitalizedWords.join('');
    capitalizedTerm = StringTool.detectCharsAndPutCharsInString(term, capitalizedTerm, '[ -_]');

    return capitalizedTerm;
  }

  static detectCharsAndPutCharsInString(firstTerm: string, secondTerm: string, chars: string): string {

    for (let i = 0; i < firstTerm.length; i++) {
      if (firstTerm[i].match(chars))
        secondTerm = StringTool.putCharInString(secondTerm, i, firstTerm[i]);
    }

    return secondTerm;
  }

  static putCharInString(term: string, index: number, char: string): string {
    return [term.slice(0, index), char, term.slice(index)].join('');
  }
}
