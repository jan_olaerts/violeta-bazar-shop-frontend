import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CookieTool {

  // Creating a cookie
  static setCookie(name: string, content: string, days: number = 0, hours: number = 0, minutes: number = 0): void {
    const now = new Date();
    const exp = new Date(now.getTime() + (((((days * 24) + hours) * 60) + minutes) * 60000));
    const expiration = exp.toUTCString();
    name = encodeURIComponent(name);
    content = encodeURIComponent(content);
    document.cookie = `${name}=${content};expires=${expiration}`;
  }

  // Getting to content of a cookie
  static getCookie(name: string): string {
    const regex: RegExp = new RegExp(name + "=([^;]+)");
    const result: RegExpExecArray = regex.exec(document.cookie);
    if (result) return decodeURIComponent(result[1]);
    else return null;
  }

  // Clearing the cookie
  static clearCookie(name: string): void {
    CookieTool.setCookie(name, "", -1);
  }
}
