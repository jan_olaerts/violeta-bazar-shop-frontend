import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Notification } from '../enum/notification.enum';
import { AuthenticationService } from '../services/authentication.service';
import { NotificationService } from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private notificationService: NotificationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    return this.isUserLoggedIn();
  }

  isUserLoggedIn(): boolean {

    if (this.authenticationService.isUserLoggedIn())
      return true;

    this.router.navigate(['/login'])
      .then(() => this.notificationService.showNotification(Notification.ERROR_COLOR, Notification.NOT_LOGGED_IN_MSG));

    return false;
  }
}
