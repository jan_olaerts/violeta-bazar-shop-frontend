import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CartItem } from 'src/app/classes/cart-item';
import { Product } from 'src/app/classes/product';
import { ProductCategory } from 'src/app/classes/product-category';
import { Notification } from 'src/app/enum/notification.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CartService } from 'src/app/services/cart.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProductCategoryService } from 'src/app/services/product-category.service';
import { ProductService } from 'src/app/services/product.service';
import { StringTool } from 'src/app/tools/stringtool';

@Component({
  selector: 'app-show-products',
  templateUrl: './show-products.component.html',
  styleUrls: ['./show-products.component.css']
})
export class ShowProductsComponent implements OnInit {

  products: Product[];
  productCategories: ProductCategory[] = new Array();
  productCategoryId: number;
  toast: string;

  constructor(private productService: ProductService,
    private productCategoryService: ProductCategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private cartService: CartService,
    private authenticatinService: AuthenticationService,
    private notificationService: NotificationService) {

    this.changeCategory();
    this.changeProductsComingFromSearchBar();
  }

  ngOnInit(): void {
    this.getProductCategoryId();
    this.navigate();
    this.getProductCategories();
  }

  navigate(): void {
    if (!this.router.url.includes('category') && !this.router.url.includes('search'))
      this.router.navigateByUrl(`category/${this.productCategoryId}`);
  }

  getProductCategoryId(): void {
    if (this.router.url.includes('category'))
      this.productCategoryId = Number.parseInt(this.route.snapshot.paramMap.get('id'));

    else if (this.router.url.includes('search'))
      this.productCategoryId = -1; // never exists, to color all links white

    else
      this.productCategoryId = 7; // category id with the first name alphabetically
  }

  getProductCategories(): void {
    this.productCategoryService.getAllProductCategories()
      .subscribe(data => {
        data.forEach(pc => {
          this.productCategories.push({ id: pc.id, categoryName: StringTool.capitalizeFirstLetters(pc.categoryName) });
        })

        this.getProducts();
      })
  }

  getProducts(): void {

    if (this.router.url.includes('category')) {
      this.productService.getProductsByCategoryId(this.productCategoryId)
        .subscribe(data => {
          this.products = data;
          this.changeScrollBarPosition();
          this.showLoadedProductsNotification();
        })
    }

    if (this.router.url.includes('search')) {
      this.productService.getProductsByKeyword(this.route.snapshot.paramMap.get('keyword'))
        .subscribe(data => {
          this.products = data;
          this.showLoadedProductsNotification();
        });
    }
  }

  showProductsByCategoryName(event): void {
    this.productCategoryId = event.target['id'];
    this.productService.getProductsByCategoryId(this.productCategoryId)
      .subscribe(data => {
        this.products = data;
      });
  }

  changeCategory(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (!this.router.url.includes('category')) return;

        this.productCategoryId = Number.parseInt(event.url.substring(10));
        this.getProducts();
        this.changeScrollBarPosition();
      }
    })
  }

  changeProductsComingFromSearchBar(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (!this.router.url.includes('search')) return;

        this.getProducts();
      }
    })
  }

  colorProductCategoryLink(event?): void {
    let productCategoryLinks: HTMLElement[] = Array.from(document.querySelectorAll('.productCategoryLink'));
    productCategoryLinks.forEach(l => l.style.color = 'white');
    if (event) event.target.style.color = 'rgba(198, 172, 237, 1)';
    else productCategoryLinks.forEach(l => { if (Number.parseInt(l['id']) === this.productCategoryId) l.style.color = 'rgba(198, 172, 237, 1)' });
    this.changeScrollBarPosition();
  }

  changeScrollBarPosition(): void {
    let productCategoriesContainer: HTMLElement = document.querySelector('#productCategoriesContainer') as HTMLElement;
    let selectedCategory: HTMLElement = document.querySelector('#productCategoriesContainer').querySelector(`a[id='${this.productCategoryId}']`);
    if (!selectedCategory) return;
    let scrollBarWidth: number = productCategoriesContainer.scrollWidth - window.innerWidth;
    let middlePointOfScrollBar: number = scrollBarWidth / 2;
    let productCategoriesContainerScrollLeft: number = productCategoriesContainer.scrollLeft;
    let xPosOfSelectedCategory: number = selectedCategory.getBoundingClientRect().left + 150;
    let scrollPos: number = xPosOfSelectedCategory - (middlePointOfScrollBar - productCategoriesContainerScrollLeft);

    productCategoriesContainer.scrollTo({ left: scrollPos, behavior: 'smooth' });
  }

  onAddButtonHover(event: Event) {
    if (window.innerWidth < 992) return;

    const btn = event.target as HTMLElement;
    btn.innerHTML = `Añadir <svg style="margin-bottom: 3px;" width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-cart-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
    <path fill-rule="evenodd" d="M11.354 5.646a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708 0z"/>
    </svg>`;
  }

  onAddButtonLeave(event: Event) {
    if (window.innerWidth < 992) return;

    const btn = event.target as HTMLElement;
    btn.innerHTML = `<svg
    style="margin-bottom: 3px;"
    width="1.2em"
    height="1.2em"
    viewBox="0 0 16 16"
    class="bi bi-cart-plus"
    fill="white"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fill-rule="evenodd"
      d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"
    />
    <path
      fill-rule="evenodd"
      d="M8.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 .5-.5z"
    />
  </svg>`;
  }

  addToCart(productId: number): void {
    let product: Product = this.products.find(p => p.id === productId);
    let cartItem: CartItem = this.cartService.cartItems.find(ci => ci.id === productId);
    if (cartItem === undefined) cartItem = new CartItem(product);
    if (!this.cartService.addToCart(cartItem)) {
      this.showNotification(Notification.ERROR_COLOR, Notification.NOT_ENOUGH_STOCK);
      return;
    };
    this.showNotification(Notification.SUCCESS_COLOR, Notification.PRODUCT_TO_CART_SUCCESS);
    this.shoppingCartAnimation();
  }

  shoppingCartAnimation(): void {
    let shoppingCartIcon: HTMLElement = document.querySelector('#shopping-cart-icon') as HTMLElement;
    shoppingCartIcon.style.animation = 'jump .3s ease forwards';
    setTimeout(() => shoppingCartIcon.style.animation = '', 300);
  }

  onEdit(id: number): void {
    this.router.navigateByUrl('/editproduct/' + id);
  }

  onDelete(id: number): void {

    this.productService.deleteProduct(id)
      .subscribe(data => {
        this.showNotification(Notification.SUCCESS_COLOR, Notification.PRODUCT_DELETE_SUCCESS);
        window.location.reload();
      }, (error: HttpErrorResponse) => this.showNotification(Notification.ERROR_COLOR, Notification.PRODUCT_DELETE_ERROR));
  }

  get isAdmin(): boolean {
    if (this.authenticatinService.getUserFromLocalStorage() == null) return false;
    return this.authenticatinService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  private showLoadedProductsNotification(): void {
    let message: string;
    if (this.products.length > 0) {
      message = this.products.length === 1 ? ' producto cargado' : ' productos cargados';
      this.showNotification(Notification.SUCCESS_COLOR, this.products.length + message);
    }
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
