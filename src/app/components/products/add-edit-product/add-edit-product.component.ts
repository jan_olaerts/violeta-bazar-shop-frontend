import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/classes/product';
import { ProductCategory } from 'src/app/classes/product-category';
import { Notification } from 'src/app/enum/notification.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProductCategoryService } from 'src/app/services/product-category.service';
import { ProductService } from 'src/app/services/product.service';
import { VioletBazarValidators } from '../../../validators/violetbazarvalidators';

@Component({
  selector: 'add-edit-product',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.css']
})
export class AddEditProductComponent implements OnInit {

  title: string;
  buttonText: string;
  product: Product;
  productCategories: ProductCategory[];
  addProductFormGroup: FormGroup;
  picture: File;
  inputFileLabel: HTMLElement;
  imageUrl: string = '';
  productId: number = 0;

  constructor(private formBuilder: FormBuilder,
    private http: HttpClient,
    private productService: ProductService,
    private productCategoryService: ProductCategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {

    this.checkIfUserAllowed();
    this.inputFileLabel = document.querySelector(".custom-file-label") as HTMLElement
    this.loadCategories();
    this.checkAddOrEdit();
    this.buildForm();
  }

  checkIfUserAllowed(): void {
    if (!this.authenticationService.isUserLoggedIn() ||
      this.authenticationService.getUserFromLocalStorage().role !== Role.ADMIN) {
      this.router.navigate(['/']);
      this.showNotification(Notification.ERROR_COLOR, Notification.URL_NOT_ALLOWED_MSG);
    }
  }

  // check for add or edit product
  checkAddOrEdit(): void {

    if (this.router.url.includes('addproduct')) {
      this.title = 'Añadir producto';
      this.buttonText = 'Añadir producto';
    }

    else if (this.router.url.includes('editproduct')) {
      this.title = 'Editar producto';
      this.buttonText = 'Editar producto';

      this.productId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
      this.getProduct(this.productId);
    }
  }

  // build the form to add a product
  buildForm(): void {
    this.addProductFormGroup = this.formBuilder.group({

      product: this.formBuilder.group({

        productName: new FormControl(this.product !== undefined ? this.product.name : '',
          [Validators.required, Validators.pattern(new RegExp(/^(?:(?!_).)*$/)), Validators.minLength(2),
          VioletBazarValidators.notOnlyWhitespace]),

        brand: new FormControl(this.product !== undefined ? this.product.brand : '',
          [Validators.required, Validators.pattern(new RegExp(/^(?:(?!_).)*$/)), Validators.minLength(2),
          VioletBazarValidators.notOnlyWhitespace]),

        category: new FormControl(this.product !== undefined ? this.product.category.categoryName : 'Categoría:',
          [VioletBazarValidators.valueCannotBeCategory]),

        price: new FormControl(this.product !== undefined ? this.product.price : '', [Validators.required, Validators.min(0)]),
        stock: new FormControl(this.product !== undefined ? this.product.stock : '', [Validators.required, Validators.min(1)]),

        description: new FormControl(this.product !== undefined ? this.product.description : '',
          [Validators.required, Validators.minLength(20)]),

        image: new FormControl(null, [Validators.required, VioletBazarValidators.mustBePngJpgOrJpeg])
      })
    });

    if (this.product !== undefined) {
      this.inputFileLabel.innerText = 'Upload Image:';
    }
  }

  // get the fields of the form
  get productName() { return this.addProductFormGroup.get('product.productName'); }
  get brand() { return this.addProductFormGroup.get('product.brand'); }
  get category() { return this.addProductFormGroup.get('product.category'); }
  get price() { return this.addProductFormGroup.get('product.price'); }
  get stock() { return this.addProductFormGroup.get('product.stock'); }
  get description() { return this.addProductFormGroup.get('product.description'); }
  get image() { return this.addProductFormGroup.get('product.image'); }

  // load the categories
  loadCategories(): void {
    this.productCategoryService.getAllProductCategories()
      .subscribe(data => {
        this.productCategories = data;
        this.buildForm();
      });
  }

  // logic for when a file is selected
  onFileSelected(event) {
    this.picture = event.target.files[0];
    this.inputFileLabel.innerText = this.picture.name;

    // show image preview
    let reader = new FileReader();
    reader.readAsDataURL(this.picture);
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
  }

  // logic for when form is submitted
  onSubmit() {

    if (this.addProductFormGroup.invalid) {
      this.addProductFormGroup.markAllAsTouched();
      return;
    }

    // make product category
    const productCategory: ProductCategory = this.makeProductCategory();

    // make product
    this.makeProduct(productCategory);

    // post or patch product
    if (this.router.url.includes('addproduct')) this.postProduct();
    else this.patchProduct();

    // make input fields empty again
    this.emptyInputFields();
  }

  // make a ProductCategory object
  makeProductCategory(): ProductCategory {
    const categoryName = this.addProductFormGroup.get('product.category').value;

    const selectedOptionField = document.evaluate(
      `//option[contains(., '${categoryName}')]`, document, null, XPathResult.ANY_TYPE, null)
      .iterateNext();

    const categoryId = selectedOptionField["id"];

    return new ProductCategory(categoryId, categoryName);
  }

  // make a Product object
  makeProduct(productCategory: ProductCategory): void {

    this.product = new Product(
      this.productId,
      this.addProductFormGroup.get('product.productName').value,
      this.addProductFormGroup.get('product.brand').value,
      this.addProductFormGroup.get('product.description').value,
      this.addProductFormGroup.get('product.price').value,
      this.addProductFormGroup.get('product.image').value,
      this.addProductFormGroup.get('product.stock').value,
      productCategory
    );
  }

  // post Product object
  postProduct(): void {

    const formData = new FormData();
    formData.append('product', JSON.stringify(this.product));
    formData.append('imageFile', this.picture, this.picture.name);

    this.productService.postProduct(formData)
      .subscribe(data => {
        this.showNotification(Notification.SUCCESS_COLOR, Notification.PRODUCT_ADD_SUCCESS);
        this.router.navigateByUrl(`/category/${this.product.category.id}`);
      }, error => {
        this.showNotification(Notification.ERROR_COLOR, Notification.PRODUCT_ADD_ERROR);
      })
  }

  // getting a product for editing
  getProduct(id: number): void {
    // getting product from service
    this.productService.getProduct(id)
      .subscribe(data => {
        this.product = data;
        this.imageUrl = `./assets/images/products/${this.product.imageUrl}`;
        this.buildForm();
      })
  }

  // patch Product object
  patchProduct(): void {

    const formData = new FormData();
    formData.append('product', JSON.stringify(this.product));
    formData.append('imageFile', this.picture, this.picture.name);

    this.productService.patchProduct(formData)
      .subscribe(data => {
        this.showNotification(Notification.SUCCESS_COLOR, Notification.PRODUCT_EDIT_SUCCESS);
        this.router.navigateByUrl(`/category/${data.category.id}`);
      }, error => {
        this.showNotification(Notification.ERROR_COLOR, Notification.PRODUCT_EDIT_ERROR);
      });
  }

  // empty form fields after submitting form
  emptyInputFields(): void {
    this.ngOnInit();
    this.inputFileLabel.innerText = 'Upload Image:';
    this.imageUrl = '';
  }

  private showNotification(color: Notification, message: Notification): void {
    this.notificationService.showNotification(color, message);
  }
}
