import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from 'src/app/classes/cart-item';
import { Product } from 'src/app/classes/product';
import { Notification } from 'src/app/enum/notification.enum';
import { CartService } from 'src/app/services/cart.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  productId: number;
  product: Product;
  imageUrl: string;

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.productId = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    this.getProduct();
  }

  getProduct(): void {
    this.productService.getProduct(this.productId)
      .subscribe(data => {
        this.product = data;
        this.imageUrl = `../assets/images/products/${this.product.imageUrl}`;
      });
  }

  addToCart(): void {
    let cartItem: CartItem = this.cartService.cartItems.find(ci => ci.id === this.productId);
    if (cartItem === undefined) cartItem = new CartItem(this.product);
    if (!this.cartService.addToCart(cartItem)) {
      this.showNotification(Notification.ERROR_COLOR, Notification.NOT_ENOUGH_STOCK);
      return;
    };
    this.showNotification(Notification.SUCCESS_COLOR, Notification.PRODUCT_TO_CART_SUCCESS);
    this.shoppingCartAnimation();
  }

  shoppingCartAnimation(): void {
    let shoppingCartIcon: HTMLElement = document.querySelector('#shopping-cart-icon') as HTMLElement;
    shoppingCartIcon.style.animation = 'jump .3s ease forwards';
    setTimeout(() => shoppingCartIcon.style.animation = '', 300);
  }

  onAddButtonHover(event: Event) {
    if (window.innerWidth < 992) return;

    const btn = event.target as HTMLElement;
    btn.innerHTML = `Añadir <svg style="margin-bottom: 3px;" width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-cart-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
    <path fill-rule="evenodd" d="M11.354 5.646a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708 0z"/>
    </svg>`;
  }

  onAddButtonLeave(event: Event) {
    if (window.innerWidth < 992) return;

    const btn = event.target as HTMLElement;
    btn.innerHTML = `Añadir <svg style="margin-bottom: 3px;" width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-cart-plus" fill="white" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
    <path fill-rule="evenodd" d="M8.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 .5-.5z"/>
    </svg>`;
  }

  private showNotification(color: Notification, message: Notification): void {
    this.notificationService.showNotification(color, message);
  }
}
