import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.animateLinks();
  }

  get isAdmin(): boolean {
    return this.authenticationService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  animateLinks(): void {
    const links = document.querySelectorAll("li");
    Array.from(links).forEach(li => li.addEventListener('mouseenter', this.showSlider));
    Array.from(links).forEach(li => li.addEventListener('mouseleave', this.hideSlider));
  }

  showSlider(event): void {
    if (window.innerWidth < 992) return;

    const slider = document.createElement('hr');
    slider.style.border = "solid 2px #c6aced";
    slider.style.borderRadius = "100px";
    slider.style.margin = "0px";
    slider.style.animation = "slide 4.5s ease forwards";
    slider.style.position = "absolute";
    slider.style.maxWidth = `${event.target.clientWidth - 20}px`;
    slider.style.transform = `translateX(8px)`;
    event.target.appendChild(slider);
  }

  hideSlider(event): void {
    if (window.innerWidth < 992) return;

    const linkChildren = Array.from(event.target.childNodes);

    if (linkChildren.length === 2)
      event.target.removeChild(linkChildren[1]);
  }

  navigate(): void {
    if (this.authenticationService.isUserLoggedIn() && !this.isAdmin)
      this.router.navigate([ProfileConstant.MY_ORDERS_URL]);

    else if (this.authenticationService.isUserLoggedIn() && this.isAdmin)
      this.router.navigate([ProfileConstant.ALL_ORDERS_URL]);

    else
      this.router.navigate(['/register']);
  }

  searchProducts(): void {
    const inputField: HTMLInputElement = document.querySelector("input[placeholder='Qué estás buscando?']");
    if (inputField.value.length === 0) return;
    this.router.navigateByUrl(`/search/${inputField.value}`);
    inputField.value = '';
  }
}
