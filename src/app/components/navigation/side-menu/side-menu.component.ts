import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ProductCategory } from 'src/app/classes/product-category';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProductCategoryService } from 'src/app/services/product-category.service';
import { StringTool } from 'src/app/tools/stringtool';

@Component({
  selector: 'side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  public menuIsOpen: boolean = menuIsOpen;
  public toggleSideMenu = toggleSideMenu;
  productCategories: ProductCategory[] = new Array();
  userButtonTitle: string;
  userLink: string;

  constructor(private productCategoryService: ProductCategoryService,
    private authenticationService: AuthenticationService,
    private router: Router) { }

  ngOnInit(): void {
    this.getProductCategories();
    this.getUserButtonTitle();
  }

  get isAdmin(): boolean {
    return this.authenticationService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  getProductCategories(): void {
    this.productCategoryService.getAllProductCategories()
      .subscribe(data => {
        data.forEach(pc => {
          this.productCategories.push({ id: pc.id, categoryName: StringTool.capitalizeFirstLetters(pc.categoryName) });
        })
      });
  }

  searchProducts(): void {
    const inputField: HTMLInputElement = document.querySelector('#search-bar-mobile').querySelector('input');
    if (inputField.value.length === 0) return;
    this.router.navigateByUrl(`/search/${inputField.value}`);
    inputField.value = '';
  }

  closeSideMenuOnScreenResize(): void {
    if (!menuIsOpen) return;
    toggleSideMenu();
  }

  getUserButtonTitle(): void {
    if (this.authenticationService.isUserLoggedIn())
      this.userButtonTitle = StringTool.capitalizeFirstLetters(
        this.authenticationService.getUserFromLocalStorage().firstNames.split(" ")[0]);

    else
      this.userButtonTitle = 'Login';
  }

  onUserNavigate(): void {
    if (this.authenticationService.isUserLoggedIn() && !this.isAdmin)
      this.router.navigate([ProfileConstant.MY_ORDERS_URL]);

    else if (this.authenticationService.isUserLoggedIn() && this.isAdmin)
      this.router.navigate([ProfileConstant.ALL_ORDERS_URL]);

    else
      this.router.navigate(['/login']);
  }
}

export var menuIsOpen: boolean = false;
export function toggleSideMenu(): void {

  const sideMenu = document.querySelector(".side-nav") as HTMLElement;
  const arrow = document.querySelector(".mobile-menu-arrow") as HTMLElement;

  if (!menuIsOpen) {
    sideMenu.style.animation = "slideOpen 1s ease forwards";
    arrow.style.animation = "rotate-180-left 1s ease forwards";
  } else {
    sideMenu.style.animation = "slideClose 1s ease forwards";
    arrow.style.animation = "rotate-180-right 1s ease forwards";
  }

  menuIsOpen = !menuIsOpen;
}
