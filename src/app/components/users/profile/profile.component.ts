import { Component, OnInit } from '@angular/core';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { ProfileAction } from 'src/app/classes/profile-action';
import { User } from 'src/app/classes/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Role } from 'src/app/enum/role.enum';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { Notification } from 'src/app/enum/notification.enum';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  actions: ProfileAction[] = [];
  selectedActionTitle: string;

  constructor(private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit(): void {
    this.getUser();
    this.fillActions();
    this.getSelectedActionTitleFromUrl();
  }

  get isAdmin(): boolean {
    return this.authenticationService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  showProfileSection(actionTitle: string): void {
    this.selectedActionTitle = actionTitle;
  }

  colorProfileLink(event?): void {
    let profileLinks: HTMLElement[] = Array.from(document.querySelectorAll('.profileLink'));
    profileLinks.forEach(l => l.style.color = 'white');
    if (event) event.target.style.color = 'rgba(198, 172, 237, 1)';
    else profileLinks.forEach(l => { if (l['id'] === this.selectedActionTitle) l.style.color = 'rgba(198, 172, 237, 1)' });
    this.changeScrollBarPosition();
  }

  private getUser(): void {
    this.user = this.authenticationService.getUserFromLocalStorage();
  }

  private fillActions(): void {
    this.getSelectedActionTitleFromUrl();
    this.actions = [
      new ProfileAction(ProfileConstant.MY_ORDERS, ProfileConstant.MY_ORDERS_URL),
      new ProfileAction(ProfileConstant.ALL_ORDERS, ProfileConstant.ALL_ORDERS_URL),
      new ProfileAction(ProfileConstant.USERS, ProfileConstant.USERS_URL),
      new ProfileAction(ProfileConstant.UPDATE_ACCOUNT, ProfileConstant.UPDATE_ACCOUNT_URL),
      new ProfileAction(ProfileConstant.LOGOUT, ProfileConstant.LOGOUT_URL)
    ];
  }

  private getSelectedActionTitleFromUrl(): void {
    if (this.router.url.includes(ProfileConstant.MY_ORDERS_URL)) {
      if (this.isAdmin) {
        this.redirectAfterUrlNotAllowed(ProfileConstant.ALL_ORDERS_URL, Notification.URL_NOT_FOR_ADMIN);
      } else {
        this.selectedActionTitle = ProfileConstant.MY_ORDERS;
      }
    }

    if (this.router.url.includes(ProfileConstant.ALL_ORDERS_URL)) {
      if (this.isAdmin) {
        this.selectedActionTitle = ProfileConstant.ALL_ORDERS;
      } else {
        this.redirectAfterUrlNotAllowed(ProfileConstant.MY_ORDERS_URL, Notification.URL_NOT_ALLOWED_MSG);
      }
    }

    if (this.router.url.includes(ProfileConstant.USERS_URL)) {
      if (this.isAdmin) {
        this.selectedActionTitle = ProfileConstant.USERS;
      } else {
        this.redirectAfterUrlNotAllowed(ProfileConstant.MY_ORDERS_URL, Notification.URL_NOT_ALLOWED_MSG);
      }
    }

    if (this.router.url.includes(ProfileConstant.UPDATE_ACCOUNT_URL))
      this.selectedActionTitle = ProfileConstant.UPDATE_ACCOUNT;

    if (this.router.url.includes(ProfileConstant.LOGOUT_URL))
      this.selectedActionTitle = ProfileConstant.LOGOUT;
  }

  private changeScrollBarPosition(): void {
    let profileContainer: HTMLElement = document.querySelector('#profileContainer') as HTMLElement;
    let selectedAction: HTMLElement = document.querySelector('#profileContainer').querySelector(`a[id='${this.selectedActionTitle}']`);
    if (!selectedAction) return;
    let scrollBarWidth: number = profileContainer.scrollWidth - window.innerWidth;
    let middlePointOfScrollBar: number = scrollBarWidth / 2;
    let profileContainerScrollLeft: number = profileContainer.scrollLeft;
    let xPosOfSelectedAction: number = selectedAction.getBoundingClientRect().left;
    let scrollPos: number = xPosOfSelectedAction - (middlePointOfScrollBar - profileContainerScrollLeft);

    profileContainer.scrollTo({ left: scrollPos, behavior: 'smooth' });
  }

  private redirectAfterUrlNotAllowed(url: any, notificationMessage: Notification) {
    this.router.navigate(url)
      .then(() => {
        setTimeout(() => {
          this.showNotification(Notification.ERROR_COLOR, notificationMessage);
        }, 1000);
      })
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
