import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { User } from 'src/app/classes/user';
import { Authority } from 'src/app/enum/authority.enum';
import { Notification } from 'src/app/enum/notification.enum';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-overview',
  templateUrl: './users-overview.component.html',
  styleUrls: ['./users-overview.component.css']
})
export class UsersOverviewComponent implements OnInit {

  selectedActionTitle: string = ProfileConstant.USERS;
  users: User[] = [];
  activeUser: User;
  incomeForActiveUser: number = 0;
  keyword: string;

  constructor(private userService: UserService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit(): void {
    this.getUsers();
    this.initProfileSection();
    this.keyword = '';
  }

  get isAdmin(): boolean {
    return this.authenticationService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  getActiveUser(userEmail: string) {
    this.activeUser = this.users.find(u => u.email === userEmail);
    this.calculateTotalIncomeForActiveUser();
  }

  closeUserInformationModal(): void {
    (<any>$('#user-information-modal')).modal('hide');
  }

  calculateTotalIncomeForActiveUser(): void {
    this.incomeForActiveUser = 0;
    this.activeUser.orders.forEach(o => {
      this.incomeForActiveUser += o.totalPrice;
    })
  }

  searchUsers(): void {
    if (this.keyword.length === 0) {
      this.getUsers();
      return;
    }

    this.users = this.users.filter(u => {
      return u.email.includes(this.keyword) ||
        u.firstNames.includes(this.keyword) ||
        u.lastName_1.includes(this.keyword) ||
        u.lastName_2.includes(this.keyword) ||
        u.dni.includes(this.keyword)
    })
  }

  clearInputField(): void {
    if (this.keyword.length === 0) {
      return;
    }

    this.keyword = '';
    this.getUsers();
  }

  private initProfileSection() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.router.url.includes(ProfileConstant.USERS_URL)) {
          this.keyword = '';
          this.getUsers();
          this.showLoadedUsersNotification();
        }
      }
    })
  }

  private getUsers(): void {

    if (!this.router.url.includes(ProfileConstant.USERS_URL)) {
      return;
    }

    if (!this.isAdmin) {
      this.showNotification(Notification.ERROR_COLOR, Notification.URL_NOT_ALLOWED_MSG);
      return;
    }
    this.userService.getUsers()
      .subscribe((data: User[]) => {
        this.users = data;
        this.showLoadedUsersNotification();
      }, (error: HttpErrorResponse) => {
        this.showNotification(Notification.ERROR_COLOR, error.message);
      })
  }

  private showLoadedUsersNotification(): void {
    let message: string;
    if (this.users.length > 0) {
      if (this.router.url.includes(ProfileConstant.USERS_URL)) {
        message = this.users.length === 1 ? ' usuario cargado' : ' usuarios cargados';
        this.showNotification(Notification.SUCCESS_COLOR, this.users.length + message);
      }

    } else {
      if (this.router.url.includes(ProfileConstant.USERS_URL))
        this.showNotification(Notification.WARNING_COLOR, Notification.NO_USERS_YET);
    }
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
