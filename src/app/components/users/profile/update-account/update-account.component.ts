import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { Address } from 'src/app/classes/address';
import { User } from 'src/app/classes/user';
import { Notification } from 'src/app/enum/notification.enum';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { CustomHttpResponse } from 'src/app/interfaces/custom-http-response';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';
import { VioletBazarValidators } from 'src/app/validators/violetbazarvalidators';

@Component({
  selector: 'app-update-account',
  templateUrl: './update-account.component.html',
  styleUrls: ['./update-account.component.css']
})
export class UpdateAccountComponent implements OnInit {

  selectedActionTitle: string = ProfileConstant.UPDATE_ACCOUNT;
  user: User;
  updatedUser: User;
  updateAccountFormGroup: FormGroup;
  showLoading: boolean;
  showResetPasswordLoading: boolean;
  password_1: string;
  password_2: string;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private authenticationService: AuthenticationService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.initProfileSection();
    this.getUser();
    this.buildForm();
    this.password_1 = '';
    this.password_2 = '';
  }

  // get the fields of the form
  get formUser() { return this.updateAccountFormGroup.get('formUser'); }
  get firstNames() { return this.updateAccountFormGroup.get('formUser.firstNames'); }
  get lastNames() { return this.updateAccountFormGroup.get('formUser.lastNames'); }
  get street() { return this.updateAccountFormGroup.get('formUser.street'); }
  get number() { return this.updateAccountFormGroup.get('formUser.number'); }
  get city() { return this.updateAccountFormGroup.get('formUser.city'); }
  get province() { return this.updateAccountFormGroup.get('formUser.province'); }
  get department() { return this.updateAccountFormGroup.get('formUser.department'); }

  onUpdateAccount(): void {
    if (this.updateAccountFormGroup.invalid) {
      this.updateAccountFormGroup.markAllAsTouched();
      return;
    }

    this.showLoading = true;
    this.makeUpdatedUser();
    this.userService.updateUser(this.updatedUser)
      .subscribe((data: User) => {
        this.showLoading = false;
        this.showNotification(Notification.SUCCESS_COLOR, Notification.ACCOUNT_UPDATE_SUCCESS);
        this.authenticationService.addUserToLocalStorage(data);
        this.getUser();
      }, (errorResponse: HttpErrorResponse) => {
        this.showLoading = false;
        this.showNotification(Notification.ERROR_COLOR, Notification.ACCOUNT_UPDATE_ERROR)
      });
  }

  onResetPassword(): void {
    if (this.password_1.length === 0 ||
      !this.password_1.match('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'),
      this.password_1 !== this.password_2) {
      this.showNotification(Notification.WARNING_COLOR, Notification.PASSWORD_NOT_VALID);
      return;
    }

    this.showResetPasswordLoading = true;
    this.userService.resetPassword(this.user.email, this.password_1)
      .subscribe((data: CustomHttpResponse) => {
        this.showResetPasswordLoading = false;
        (<any>$('#reset-password-modal')).modal('hide');
        this.showNotification(Notification.SUCCESS_COLOR, Notification.PASSWORD_EDIT_SUCCESS);
      }, (errorResponse: HttpErrorResponse) => {
        this.showResetPasswordLoading = false;
        this.showNotification(Notification.ERROR_COLOR, Notification.PASSWORD_EDIT_ERROR)
      });
  }

  private initProfileSection(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.router.url.includes(ProfileConstant.UPDATE_ACCOUNT_URL)) {
          this.buildForm();
          this.password_1 = '';
          this.password_2 = '';
        }
      }
    })
  }

  private getUser(): void {
    this.user = this.authenticationService.getUserFromLocalStorage();
  }

  private buildForm(): void {
    this.updateAccountFormGroup = this.formBuilder.group({

      formUser: this.formBuilder.group({

        firstNames: new FormControl(this.user.firstNames, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace, VioletBazarValidators.mustBeExactlyTwoWords]),
        lastNames: new FormControl(this.user.lastName_1 + ' ' + this.user.lastName_2, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace, VioletBazarValidators.mustBeExactlyTwoWords]),
        street: new FormControl(this.user.address.street, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        number: new FormControl(this.user.address.number, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        city: new FormControl(this.user.address.city, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        province: new FormControl(this.user.address.province, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        department: new FormControl(this.user.address.department, [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace])
      })

    });
  }

  private makeUpdatedUser(): void {
    this.updatedUser = new User(
      this.user.email,
      '  ',
      this.user.dni,
      this.firstNames.value.trim().toUpperCase(),
      this.lastNames.value.split(' ')[0].trim().toUpperCase(),
      this.lastNames.value.split(' ')[1].trim().toUpperCase(),
      this.makeUpdatedAddress(),
      this.user.orders
    );
  }

  private makeUpdatedAddress(): Address {
    return new Address(
      0,
      this.street.value.trim().toUpperCase(),
      this.number.value.trim().toUpperCase(),
      this.city.value.trim().toUpperCase(),
      this.province.value.trim().toUpperCase(),
      this.department.value.trim().toUpperCase()
    );
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
