import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Notification } from 'src/app/enum/notification.enum';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  selectedActionTitle: string = ProfileConstant.LOGOUT;

  constructor(private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit(): void {
  }

  onLogout(wantsToLogout: boolean): void {
    if (!wantsToLogout) {
      this.router.navigate(['/'])
        .then(() => {
          setTimeout(() => {
            this.showNotification(Notification.SUCCESS_COLOR, Notification.NOT_LOGGED_OUT_MSG);
          }, 1000);
        });
      return;
    }

    this.authenticationService.logout();
    this.router.navigate(['/login'])
      .then(() => this.showNotification(Notification.WARNING_COLOR, Notification.LOGGED_OUT_MSG));
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
