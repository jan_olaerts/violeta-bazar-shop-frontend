import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { Order } from 'src/app/classes/order';
import { User } from 'src/app/classes/user';
import { Notification } from 'src/app/enum/notification.enum';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-orders-overview',
  templateUrl: './orders-overview.component.html',
  styleUrls: ['./orders-overview.component.css']
})
export class OrdersOverviewComponent implements OnInit {

  user: User;
  orders: Order[] = [];
  activeOrder: Order;
  activeOrderIndex = 0;
  totalOrderPrice: number = 0;
  selectedActionTitle: string;
  email: string;

  constructor(private notificationService: NotificationService,
    private authenticationService: AuthenticationService,
    private orderService: OrderService,
    private router: Router) {

    this.selectedActionTitle = this.isAdmin ? ProfileConstant.ALL_ORDERS : ProfileConstant.MY_ORDERS;
  }

  ngOnInit(): void {
    this.getUser();
    this.getOrders();
    this.initProfileSection();
    this.email = '';
  }

  get isAdmin(): boolean {
    return this.authenticationService.getUserFromLocalStorage().role === Role.ADMIN;
  }

  goUpOrderIndex(): void {
    if (this.activeOrderIndex >= this.orders.length - 1) {
      this.showNotification(Notification.ERROR_COLOR, Notification.LAST_ORDER_REACHED);
      return;
    }
    this.activeOrderIndex += 1;
    this.activeOrder = this.orders[this.activeOrderIndex];
  }

  orderIndexSelected(index: number) {
    this.activeOrderIndex = index;
    this.activeOrder = this.orders[this.activeOrderIndex];
  }

  goDownOrderIndex(): void {
    if (this.activeOrderIndex <= 0) {
      this.showNotification(Notification.ERROR_COLOR, Notification.FIRST_ORDER_REACHED);
      return;
    }
    this.activeOrderIndex -= 1;
    this.activeOrder = this.orders[this.activeOrderIndex];
  }

  onOrderPaginationLinkMouseOver(index: number): void {
    const orderPaginationLink: HTMLAnchorElement = document.evaluate(
      `//a[contains(., '${index}')]`, document, null, XPathResult.ANY_TYPE, null)
      .iterateNext() as HTMLAnchorElement;

    orderPaginationLink.style.background = 'rgba(198, 172, 237, 1)';
    orderPaginationLink.style.color = 'white';
  }

  onOrderPaginationLinkMouseLeave(index: number): void {
    const orderPaginationLink: HTMLAnchorElement = document.evaluate(
      `//a[contains(., '${index}')]`, document, null, XPathResult.ANY_TYPE, null)
      .iterateNext() as HTMLAnchorElement;

    orderPaginationLink.style.background = 'white';
    orderPaginationLink.style.color = 'rgba(198, 172, 237, 1)';
  }

  closeSendInformationModal(): void {
    (<any>$('#orderSendInformationModal')).modal('hide');
  }

  searchOrdersByEmail(): void {

    if (this.email.length === 0) {
      this.getOrders();
      return;
    };

    if (!this.email.match(new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/))) {
      this.showNotification(Notification.ERROR_COLOR, Notification.EMAIL_NOT_VALID);
      this.email = '';
      return;
    };

    this.orderService.getOrdersByUserEmail(this.email)
      .subscribe((data: Order[]) => {
        this.orders = data;
        this.showLoadedOrdersNotification();
        if (this.orders.length === 0) {
          setTimeout(() => {
            this.email = '';
            this.getOrders();
          }, 1500);
        }
      });
  }

  private initProfileSection(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.router.url.includes(ProfileConstant.MY_ORDERS_URL) || this.router.url.includes(ProfileConstant.ALL_ORDERS_URL)) {
          this.email = '';
          this.getUser();
          this.getOrders();
          this.activeOrderIndex = 0;
          this.getActiveOrder();
          this.showLoadedOrdersNotification();
        }
      }
    })
  }

  private getUser(): void {
    this.user = this.authenticationService.getUserFromLocalStorage();
  }

  private getOrders(): void {

    if (this.isAdmin) {
      this.orderService.getAllOrders()
        .subscribe((data: Order[]) => {
          this.orders = data;
          this.showLoadedOrdersNotification();
        }, (error: HttpErrorResponse) => {
          this.showNotification(Notification.ERROR_COLOR, Notification.ORDER_LOAD_ERROR);
        }, () => this.getActiveOrder());
    } else {
      this.orderService.getOrdersByUserEmail(this.user.email)
        .subscribe((data: Order[]) => {
          this.orders = data;
          this.showLoadedOrdersNotification();
        }, (error: HttpErrorResponse) => {
          this.showNotification(Notification.ERROR_COLOR, Notification.ORDER_LOAD_ERROR);
        }, () => this.getActiveOrder());
    }
  }

  private getActiveOrder(): void {
    if (this.orders.length === 0) {
      this.activeOrder == null;
      return;
    }

    this.activeOrder = this.orders[this.activeOrderIndex];
  }

  private showLoadedOrdersNotification(): void {
    let message: string;
    if (this.orders.length > 0) {
      if (this.router.url.includes(ProfileConstant.MY_ORDERS_URL) || this.router.url.includes(ProfileConstant.ALL_ORDERS_URL)) {
        message = this.orders.length === 1 ? ' orden cargada' : ' ordenes cargadas';
        this.showNotification(Notification.SUCCESS_COLOR, this.orders.length + message);
      }

    } else {
      if (this.router.url.includes(ProfileConstant.MY_ORDERS_URL))
        this.showNotification(Notification.WARNING_COLOR, Notification.NO_ORDERS_YET);

      else if (this.router.url.includes(ProfileConstant.ALL_ORDERS_URL))
        this.showNotification(Notification.WARNING_COLOR, Notification.NO_ORDERS_FOUND);
    }
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
