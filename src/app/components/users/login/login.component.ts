import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/classes/user';
import { HeaderType } from 'src/app/enum/header-type.enum';
import { Notification } from 'src/app/enum/notification.enum';
import { ProfileConstant } from 'src/app/enum/profile-constant.enum';
import { Role } from 'src/app/enum/role.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { VioletBazarValidators } from 'src/app/validators/violetbazarvalidators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginFormGroup: FormGroup;
  showLoading: boolean;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.checkIfUserLoggedIn();
    this.buildForm();
  }

  checkIfUserLoggedIn(): void {
    if (this.authenticationService.isUserLoggedIn()) {
      this.router.navigate(['/'])
        .then(() => {
          setTimeout(() => {
            this.showNotification(Notification.ERROR_COLOR, Notification.ALREADY_LOGGED_IN_MSG);
          }, 1000);
        })
    }

    else
      this.router.navigate(['/login']);
  }

  buildForm(): void {
    this.loginFormGroup = this.formBuilder.group({
      userData: this.formBuilder.group({
        email: new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/)), VioletBazarValidators.notOnlyWhitespace]),
        password: new FormControl('', [Validators.required, VioletBazarValidators.notOnlyWhitespace])
      })
    });
  }

  get userData() { return this.loginFormGroup.get('userData'); }
  get email() { return this.loginFormGroup.get('userData.email'); }
  get password() { return this.loginFormGroup.get('userData.password'); }

  onLogin(): void {

    if (this.loginFormGroup.invalid) {
      this.loginFormGroup.markAllAsTouched();
      return;
    }

    this.showLoading = true;
    this.makeLoginCall();
  }

  private makeLoginCall(): void {
    this.authenticationService.login(this.email.value, this.password.value)
      .subscribe((data: HttpResponse<User>) => {
        const token = data.headers.get(HeaderType.JWT_TOKEN);
        this.authenticationService.saveToken(token);
        this.authenticationService.addUserToLocalStorage(data.body);
        this.showLoading = false;
        this.showNotification(Notification.SUCCESS_COLOR, Notification.LOGIN_SUCCESS);
        if (data.body.role == Role.USER) this.router.navigate([ProfileConstant.MY_ORDERS_URL]);
        if (data.body.role == Role.ADMIN) this.router.navigate([ProfileConstant.ALL_ORDERS_URL]);
      }, (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse);
        this.showNotification(Notification.ERROR_COLOR, errorResponse.error.message);
        this.showLoading = false;
      })
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
