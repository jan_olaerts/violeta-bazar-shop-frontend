import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from 'src/app/classes/address';
import { User } from 'src/app/classes/user';
import { Notification } from 'src/app/enum/notification.enum';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';
import { VioletBazarValidators } from 'src/app/validators/violetbazarvalidators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerFormGroup: FormGroup;
  firstNames: string;
  lastName_1: string;
  lastName_2: string;
  user: User;
  showLoading: boolean;
  isPersonInfoCorrect: boolean;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.checkIfUserLoggedIn();
    this.buildForm();
  }

  checkIfUserLoggedIn(): void {
    if (this.authenticationService.isUserLoggedIn()) {
      this.router.navigate(['/'])
        .then(() => {
          setTimeout(() => {
            this.showNotification(Notification.ERROR_COLOR, Notification.ALREADY_LOGGED_IN_MSG);
          }, 1000);
        })
    }

    else
      this.router.navigate(['/register']);
  }

  buildForm(): void {
    this.registerFormGroup = this.formBuilder.group({

      formUser: this.formBuilder.group({

        dni: new FormControl('', [Validators.required, Validators.pattern(new RegExp(/^\d{8}$/))]),
        street: new FormControl('', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        number: new FormControl('', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        city: new FormControl('', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        province: new FormControl('', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        department: new FormControl('', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        email: new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/)), VioletBazarValidators.notOnlyWhitespace]),
        password: new FormControl('', [Validators.required, Validators.pattern(new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/)), VioletBazarValidators.notOnlyWhitespace])
      })

    });
  }

  // get the fields of the form
  get formUser() { return this.registerFormGroup.get('formUser'); }
  get dni() { return this.registerFormGroup.get('formUser.dni'); }
  get street() { return this.registerFormGroup.get('formUser.street'); }
  get number() { return this.registerFormGroup.get('formUser.number'); }
  get city() { return this.registerFormGroup.get('formUser.city'); }
  get province() { return this.registerFormGroup.get('formUser.province'); }
  get department() { return this.registerFormGroup.get('formUser.department'); }
  get email() { return this.registerFormGroup.get('formUser.email'); }
  get password() { return this.registerFormGroup.get('formUser.password'); }

  onRegister(): void {

    if (this.registerFormGroup.invalid) {
      this.registerFormGroup.markAllAsTouched();
      return;
    }

    this.showLoading = true;
    this.getPerson();
  }

  onVerifyPerson(personInfoCorrect: boolean): void {
    this.isPersonInfoCorrect = personInfoCorrect;
    this.showLoading = false;
    if (this.isPersonInfoCorrect) {
      this.showLoading = true;
      this.user = this.makeUser();
      this.makeRegisterCall();
    }
  }

  private getPerson(): void {

    this.userService.getPerson(this.dni.value).subscribe((data: any) => {

      if (data.success) {
        this.firstNames = data.data.nombres;
        this.lastName_1 = data.data.apellido_paterno;
        this.lastName_2 = data.data.apellido_materno;

        const modalButton: HTMLButtonElement = document.querySelector('#modalButton') as HTMLButtonElement;
        modalButton.click();
      } else {
        this.showLoading = false;
        this.showNotification(Notification.ERROR_COLOR, Notification.DNI_DOES_NOT_EXIST);
      }

    }, (errorResponse: HttpErrorResponse) => {
      this.showNotification(Notification.ERROR_COLOR, errorResponse.error.message);
      this.showLoading = false;
    })
  }

  private makeUser(): User {
    return new User(
      this.email.value.trim(),
      this.password.value.trim(),
      this.dni.value.trim(),
      this.firstNames.trim().toUpperCase(),
      this.lastName_1.trim().toUpperCase(),
      this.lastName_2.trim().toUpperCase(),
      this.makeAddress(),
      []);
  }

  private makeAddress(): Address {
    return new Address(
      0,
      this.street.value.trim().toUpperCase(),
      this.number.value.trim().toUpperCase(),
      this.city.value.trim().toUpperCase(),
      this.province.value.trim().toUpperCase(),
      this.department.value.trim().toUpperCase(),
    );
  }

  private makeRegisterCall(): void {
    this.authenticationService.register(this.user)
      .subscribe((data: User) => {
        this.user = data;
        this.showLoading = false;
        this.showNotification(Notification.SUCCESS_COLOR, `Hola, ${this.user.firstNames}, verifique tu email`);
        this.router.navigate(['/login'])
          .then(() => this.showNotification(Notification.SUCCESS_COLOR, Notification.REGISTER_SUCCES));
      }, (errorResponse: HttpErrorResponse) => {
        this.showLoading = false;
        this.showNotification(Notification.ERROR_COLOR, errorResponse.error.message);
      });
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
