import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from 'src/app/classes/address';
import { CartItem } from 'src/app/classes/cart-item';
import { Order } from 'src/app/classes/order';
import { Product } from 'src/app/classes/product';
import { User } from 'src/app/classes/user';
import { Notification } from 'src/app/enum/notification.enum';
import { Payment } from 'src/app/enum/payment.enum';
import { CustomHttpResponse } from 'src/app/interfaces/custom-http-response';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CartService } from 'src/app/services/cart.service';
import { NotificationService } from 'src/app/services/notification.service';
import { OrderService } from 'src/app/services/order.service';
import { PaymentService } from 'src/app/services/payment.service';
import { ProductService } from 'src/app/services/product.service';
import { VioletBazarValidators } from 'src/app/validators/violetbazarvalidators';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  sendInformationFormGroup: FormGroup;
  cartItems: CartItem[] = this.cartService.getCartItemsFromLocalStorage();
  totalPrice: number = 0;
  totalQuantity: number = 0;
  user: User;
  sendAddress: Address;
  allowSendInfoChange: boolean = false;
  showLoading: boolean;
  order: Order;

  constructor(private cartService: CartService,
    private authenticationService: AuthenticationService,
    private productService: ProductService,
    private orderService: OrderService,
    private notificationService: NotificationService,
    private paymentService: PaymentService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listCartDetails();
    this.buildSendInformationForm();
    this.sendInformationFormGroup.controls['sendInformation'].disable();
    this.checkPaymentStatus();
  }

  // get send information form fields
  get sendInformation() { return this.sendInformationFormGroup.get('sendInformation'); }
  get send_firstNames() { return this.sendInformationFormGroup.get('sendInformation.send_firstNames'); }
  get send_lastNames() { return this.sendInformationFormGroup.get('sendInformation.send_lastNames'); }
  get send_street() { return this.sendInformationFormGroup.get('sendInformation.send_street'); }
  get send_number() { return this.sendInformationFormGroup.get('sendInformation.send_number'); }
  get send_city() { return this.sendInformationFormGroup.get('sendInformation.send_city'); }
  get send_province() { return this.sendInformationFormGroup.get('sendInformation.send_province'); }
  get send_department() { return this.sendInformationFormGroup.get('sendInformation.send_department'); }

  incrementQuantity(cartItem: CartItem): void {
    if (!this.cartService.addToCart(cartItem))
      this.showNotification(Notification.ERROR_COLOR, Notification.NOT_ENOUGH_STOCK);

    this.listCartDetails();
  }

  decrementQuantity(cartItem: CartItem): void {
    this.cartService.decrementQuantity(cartItem);
    this.listCartDetails();
  }

  onPayButtonClick(): void {
    if (!this.authenticationService.isUserLoggedIn()) {
      this.router.navigate(['/login'])
        .then(() => {
          this.showNotification(Notification.ERROR_COLOR, Notification.NOT_LOGGED_IN_MSG);
        })
      return;
    }

    this.showLoading = true;
    this.sendInformationFormGroup.controls['sendInformation'].disable();
    this.getProductsToCheckStock();
  }

  onChangeSendInfo(): void {
    this.allowSendInfoChange = true;
    this.sendInformationFormGroup.controls['sendInformation'].enable();
    this.emptyAllSendInformationFormFields();
  }

  onSaveSendAddress(): void {
    if (this.sendInformationFormGroup.invalid) {
      this.sendInformationFormGroup.markAllAsTouched();
      return;
    }

    this.makeSendAddress();
    (<any>$('#sendInformationModal')).modal('hide');
    this.onPay();
  }

  onCancelPayment(): void {
    this.buildSendInformationForm();
    this.allowSendInfoChange = false;
    this.showLoading = false;
  }

  private listCartDetails(): void {
    this.cartItems = this.cartService.getCartItemsFromLocalStorage();
    if (this.cartItems == null) this.cartItems = [];
    if (this.cartItems.length === 0) this.showNotification(Notification.WARNING_COLOR, Notification.NO_PRODUCTS_IN_CART);

    // subscribe to the cart totalPrice
    this.cartService.totalPrice.subscribe(data => this.totalPrice = data);

    // subscribe to the cart totalQuantity
    this.cartService.totalQuantity.subscribe(data => this.totalQuantity = data);

    this.cartService.computeCartTotals();
  }

  private getProductsToCheckStock(): void {

    let products: Product[] = [];

    this.productService.getProductsByCartItems(this.cartItems)
      .subscribe((data: Product[]) => {
        products = data;
        this.checkStockWithProducts(products);
      });
  }

  private checkStockWithProducts(products: Product[]): void {

    let enoughStock: boolean = true;
    for (let i: number = 0; i < products.length; i++) {
      if (products[i].stock < this.cartItems[i].quantity) {
        this.showNotification(Notification.ERROR_COLOR, Notification.NOT_ENOUGH_STOCK);
        enoughStock = false;
        break;
      }
    }

    if (!enoughStock) {
      this.showLoading = false;
      return;
    }
    (<any>$('#sendInformationModal')).modal('show');
    this.buildSendInformationForm();
  }

  private buildSendInformationForm(): void {
    this.user = this.authenticationService.getUserFromLocalStorage();

    this.sendInformationFormGroup = this.formBuilder.group({
      sendInformation: this.formBuilder.group({

        send_firstNames: new FormControl(this.user ? this.user.firstNames : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_lastNames: new FormControl(this.user ? this.user.lastName_1 + ' ' + this.user.lastName_2 : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_street: new FormControl(this.user ? this.user.address.street : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_number: new FormControl(this.user ? this.user.address.number : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_city: new FormControl(this.user ? this.user.address.city : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_province: new FormControl(this.user ? this.user.address.province : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace]),
        send_department: new FormControl(this.user ? this.user.address.department : '', [Validators.required, Validators.minLength(2), VioletBazarValidators.notOnlyWhitespace])
      })
    })
  }

  private emptyAllSendInformationFormFields(): void {
    this.sendInformation.setValue({
      send_firstNames: '',
      send_lastNames: '',
      send_street: '',
      send_number: '',
      send_city: '',
      send_province: '',
      send_department: '',
    });
  }

  private makeSendAddress(): void {
    this.sendAddress = new Address(
      0,
      this.send_street.value.trim().toUpperCase(),
      this.send_number.value.trim().toUpperCase(),
      this.send_city.value.trim().toUpperCase(),
      this.send_province.value.trim().toUpperCase(),
      this.send_department.value.trim().toUpperCase()
    );
  }

  private makeOrder(): void {
    this.makeSendAddress();
    this.order = new Order(
      0,
      new Date(),
      this.user,
      this.send_firstNames.value.trim().toUpperCase(),
      this.send_lastNames.value.trim().toUpperCase(),
      this.sendAddress,
      this.cartItems,
      this.totalPrice
    );
    this.orderService.saveOrderToLocalStorage(this.order);
  }

  private onPay(): void {
    this.makeOrder();
    this.makeGetPaymentUrlCall();
  }

  private makeGetPaymentUrlCall(): void {
    let paymentUrl: string;
    this.paymentService.getPaymentUrl(this.user, this.cartItems)
      .subscribe((data: string) => {
        paymentUrl = data;
        window.location.replace(paymentUrl);
      }, (errorResponse: HttpErrorResponse) => {
        this.showLoading = false;
        console.log(errorResponse);
      })
  }

  private makeOrderCall(paymentId: string): void {
    this.orderService.placeOrder(this.order, paymentId).subscribe();
  }

  private checkPaymentStatus(): void {
    if (this.router.url.includes(Payment.PAYMENT_ERROR_QUERY_STRING)) {
      this.onPaymentFail();
    }

    if (this.router.url.includes(Payment.PAYMENT_PENDING_QUERY_STRING)) {
      this.onPaymentPending();
    }

    if (this.router.url.includes(Payment.PAYMENT_SUCCESS_QUERY_STRING)) {
      this.onPaymentSuccess();
    }
  }

  private onPaymentFail(): void {
    this.showLoading = false;
    this.showNotification(Notification.ERROR_COLOR, Notification.PAYMENT_ERROR);
    this.router.navigate(['shoppingcart']);
  }

  private onPaymentPending(): void {
    this.showLoading = false;
    let paymentId: string = this.router.parseUrl(this.router.url).queryParams['payment_id'] || '';
    this.paymentService.cancelPayment(this.user.firstNames.split(" ")[0], this.user.email, paymentId)
      .subscribe((data: CustomHttpResponse) => {
        this.showNotification(Notification.ERROR_COLOR, data.message);
        this.router.navigate(['shoppingcart']);
      });
  }

  private onPaymentSuccess(): void {
    this.showLoading = false;
    let paymentId: string = this.router.parseUrl(this.router.url).queryParams['payment_id'] || '';
    this.showNotification(Notification.SUCCESS_COLOR, Notification.PAYMENT_SUCCESS);
    this.order = this.orderService.getOrderFromLocalStorage();
    this.orderService.deleteOrderFromLocalStorage();
    this.makeOrderCall(paymentId);
    this.cartItems = [];
    this.cartService.deleteCartItemsFromLocalStorage();
    this.router.navigate(['shoppingcart']);
  }

  private showNotification(color: Notification, message: string): void {
    this.notificationService.showNotification(color, message);
  }
}
